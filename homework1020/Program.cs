﻿using System;

namespace topic1
{

    class MyStack<T>//泛型类MyStack<T>
    {
        private T[] Items;
        private int top;

        public MyStack(int size)
        {
            Items = new T[size];
            top = -1; //将顶部初始化为-1，表示有一个空的堆栈。
        }

        public void InitStack()//初始化InitStack
        {
            top = -1;
        }

        public void ClearStack()//清空栈ClearStack
        {
            top = -1;
            Array.Clear(Items, 0, Items.Length);
        }

        public bool Push(T item)//入栈Push 
        {
            if (top < Items.Length - 1)
            {
                top++;
                Items[top] = item;
                return true;
            }
            else
            {
                Console.WriteLine("堆栈已满。无法输入");
                return false;
            }
        }

        public T Pop()//出栈（删除栈顶元素） Pop 
        {
            if (top >= 0)
            {
                T poppedItem = Items[top];
                top--;
                return poppedItem;
            }
            else
            {
                Console.WriteLine("堆栈为空。无法返回");
                return default(T);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyStack<int> intStack = new MyStack<int>(5);
            intStack.Push(1);
            intStack.Push(2);
            intStack.Push(3);

            Console.WriteLine("Popped: " + intStack.Pop());

            intStack.ClearStack();

            Console.ReadLine(); //保持窗口
        }
    }

}