﻿using System;
using System.Collections.Generic;

namespace topic2
{
    class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }

    class Classroom
    {
        private List<Student> students;

        public Classroom()
        {
            students = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            students.Add(student);
        }

        public Student this[int index]
        {
            get
            {
                if (index >= 0 && index < students.Count)
                {
                    return students[index];
                }
                else
                {
                    throw new IndexOutOfRangeException("数值不符合");
                }
            }
        }

        public int StudentCount
        {
            get { return students.Count; }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Classroom myClassroom = new Classroom();

            // 添加学生到班级
            myClassroom.AddStudent(new Student("张三", 21));
            myClassroom.AddStudent(new Student("李四", 22));
            myClassroom.AddStudent(new Student("王二", 20));

            // 访问班级中的学生并输出信息
            for (int i = 0; i < myClassroom.StudentCount; i++)
            {
                Student student = myClassroom[i];
                Console.WriteLine($"学生 {i + 1}: 姓名：{student.Name}, 年龄：{student.Age}");
            }

            Console.ReadLine(); //保持窗口
        }
    }

}