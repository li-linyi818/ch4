﻿using System;


namespace topic4
{
    // 气象站类
    public class WeatherStation
    {
        public DateTime LastUpdate { get; private set; }
        public double Temperature { get; set; }
        public double Humidity { get; set; }

        public event EventHandler WeatherDataChanged;//通知数据变化

        public void UpdateWeatherData(double temperature, double humidity)//更新数据
        {
            Temperature = temperature;
            Humidity = humidity;
            LastUpdate = DateTime.Now;

            OnWeatherDataChanged();
        }

        protected virtual void OnWeatherDataChanged()
        {
            WeatherDataChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    // 屏幕显示类
    public class ScreenDisplay//显示气象数据
    {
        public void DisplayWeatherData(WeatherStation station)
        {
            Console.WriteLine("气象站数据:");
            Console.WriteLine($"更新时间: {station.LastUpdate}");
            Console.WriteLine($"温度: {station.Temperature} °C");
            Console.WriteLine($"湿度: {station.Humidity}%\n");
        }
    }

    // 数据库保存类
    public class DatabaseSaver//保存数据
    {
        public void SaveWeatherData(WeatherStation station)
        {
            //将数据保存到数据库中
            Console.WriteLine("正在保存天气数据");
            Console.WriteLine($"更新时间: {station.LastUpdate}");
            Console.WriteLine($"温度: {station.Temperature} °C");
            Console.WriteLine($"湿度 {station.Humidity}%");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            WeatherStation weatherStation = new WeatherStation();
            ScreenDisplay screenDisplay = new ScreenDisplay();
            DatabaseSaver databaseSaver = new DatabaseSaver();

            // 订阅气象站数据变化事件
            weatherStation.WeatherDataChanged += (sender, e) =>
            {
                screenDisplay.DisplayWeatherData(weatherStation);
                databaseSaver.SaveWeatherData(weatherStation);
            };

            // 模拟更新气象数据，此时，数据变化事件将触发，屏幕显示和数据库保存将自动更新。
            weatherStation.UpdateWeatherData(23.0, 66.6);

            Console.ReadLine(); //保持窗口
        }
    }

}