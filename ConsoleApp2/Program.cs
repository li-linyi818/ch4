﻿using System;

namespace topic3
{

    public delegate bool GradePrint(Student s);

    public class Student
    {
        public GradePrint GP { get; set; }
        public string Name { get; set; }
        public int[] Grades { get; set; }

        public Student(string name, int[] grades)
        {
            Name = name;
            Grades = grades;
        }

        public void PrintGradeReport()
        {
            if (GP != null)
            {
                bool result = GP(this);
                if (result)
                {
                    Console.WriteLine("成绩打印成功。");
                }
                else
                {
                    Console.WriteLine("成绩打印失败。");
                }
            }
        }
    }

    public class GradeReport
    {
        public static bool GradeReportOrderByTerm(Student s)
        {
            Console.WriteLine(s.Name + "的成绩报告如下：");
            Console.WriteLine("课程\t成绩");
            for (int i = 0; i < s.Grades.Length; i++)
            {
                Console.WriteLine($"课程{i + 1} :\t{s.Grades[i]}");
            }
            return true; //打印完成返回true
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student("张三", new int[] { 99, 88, 77, 66 });
            student.GP = GradeReport.GradeReportOrderByTerm;
            student.PrintGradeReport();
        }
    }

}